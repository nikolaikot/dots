Clone the dots repository into your home directory (add ssh key if needed)

Add symlincs for files which you need (vim example)
ln -s /home/nik/dots/.vimrc .vimrc

Enjoy!
