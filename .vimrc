"        _
" __   _(_)_ __ ___  _ __ ___
" \ \ / / | '_ ` _ \| '__/ __|
"  \ V /| | | | | | | | | (__
"   \_/ |_|_| |_| |_|_|  \___|
"
" Nik's .vimrc file

" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim'))
silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" disable automatic comment insertion
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

let mapleader = ' '
nnoremap <leader>q :q<CR>
nnoremap <leader>w :w<CR>

" Behavior {{{
  silent! set mouse=nic           " use the mouse, but not in insert or visual mode
  set autoread                    " don't bother me hen a file changes
  set scroll=5                    " number of lines to scroll with ^U/^D
  set scrolloff=5                 " keep cursor away from this many chars top/bot
  set ruler                       " show row/col and percentage
  set showcmd                     " show uncompleted command in bottom bar
  set wildmenu                    " show possible completions on command line
  set wildmode=list:longest,full  " list all options and complete
  set encoding=utf8               " UTF-8 by default
  set formatoptions-=cro          " do not format comments
  "set paste                       " awoid unexpected effects when pasting from clipboard
  set showmatch                   " highlight matching [{()}]
  if &term =~ '^xterm'            " make nice cursor in WSL
    " Cursor in terminal:
    " Link: https://vim.fandom.com/wiki/Configuring_the_cursor
    " 0 -> blinking block not working in wsl
    " 1 -> blinking block
    " 2 -> solid block
    " 3 -> blinking underscore
    " 4 -> solid underscore
    " Recent versions of xterm (282 or above) also support
    " 5 -> blinking vertical bar
    " 6 -> solid vertical bar


    " normal mode
    let &t_EI .= "\e[1 q"
    " insert mode
    let &t_SI .= "\e[5 q"

    augroup windows_term
      autocmd!
      autocmd VimEnter * silent !echo -ne "\e[1 q"
      autocmd VimLeave * silent !echo -ne "\e[5 q"
    augroup END
  endif
" }}}

" Colors {{{
  set background=dark             " tell vim what background you are using
  let &colorcolumn="80,".join(range(120,999),",")
                                  " set width reminders
  set cursorline                  " highlight current line
  highlight ColorColumn ctermbg=236 guibg=#2c2d27
                                  " set colors of reminders
  highlight CursorLine ctermbg=236
                                  " set current line background color
" }}}

" UI Layout {{{
  set list                        " show whitespace as special chars
                                  "- see listchars
  set listchars=tab:»\ ,extends:›,precedes:‹,nbsp:·,trail:·
                                  " unicode characters for various things
  set number                      " show real line number at current pos.
  set linebreak                   " break long lines by word, not char
  set lazyredraw                  " redraw only when we need to
" }}}

" Searching {{{
  set ignorecase                  " case insensitive
  set smartcase                   " lets you search for ALL CAPS
  set incsearch                   " search as you type
" }}}

" Spaces & Tabs {{{
  set expandtab                   " use spaces instead of tabs
  set smarttab                    " enable smart tabs
  set autoindent                  " carry over indenting from previous line
  set smartindent                 " enable smart autoindenting
  set tabstop=2                   " the One True Tab
  set softtabstop=2               " spaces 'feel' like tabs
  set shiftwidth=2                " number of spaces to shift for autoindent or >,<
" }}}

" Folding {{{
  set foldmethod=indent           " fold based on indent level
  set nofoldenable                " don't fold by default on open
  set foldlevel=99                " prevent closing all folds for the first time
"}}}

" Formatting {{{
  command! TW %s/\s\+$//          " Trim trailing Whitespaces
  command! CL retab | TW          " retab with spaces and trim (CLean)
" }}}

" Move Shortcuts {{{
  nnoremap j gj
  nnoremap k gk
  nnoremap J 5j
  nnoremap K 5k
  nnoremap <leader>j J
" }}}

" Move 1 more lines up or down in normal and visual selection modes.
"nnoremap K :m .-2<CR>==
"nnoremap J :m .+1<CR>==
"vnoremap K :m '<-2<CR>gv=gv
"vnoremap J :m '>+1<CR>gv=gv

" Copy to clipboard in WSL {{{
  " WSL yank support
  let s:clip = '/mnt/c/Windows/System32/clip.exe'  " change this path according to your mount point
  if executable(s:clip)
    augroup WSLYank
      autocmd!
      autocmd TextYankPost * if v:event.operator ==# 'y' | call system(s:clip, @0) | endif
    augroup END
  endif
"}}}

" polyglot {{{
  let g:polyglot_disabled = ['autoindent']
                                " do not attempt indent newly opened file
"}}}

" NerdTree {
  nnoremap <leader>n :NERDTreeFocus<CR>
  nnoremap <C-n> :NERDTree<CR>
  nnoremap <leader>t :NERDTreeToggle<CR>
  nnoremap <C-f> :NERDTreeFind<CR>

  " Exit Vim if NERDTree is the only window left.
  autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
" }

" Vim Plug {{{
  call plug#begin('~/.vim/plugged')

  Plug 'jiangmiao/auto-pairs'
  Plug 'tpope/vim-surround'
  Plug 'sheerun/vim-polyglot'
  Plug 'scrooloose/nerdtree'
  Plug 'Xuyuanp/nerdtree-git-plugin'
  Plug 'gruvbox-community/gruvbox'


  call plug#end()
" }}}
colorscheme gruvbox             " specifiy a color scheme
